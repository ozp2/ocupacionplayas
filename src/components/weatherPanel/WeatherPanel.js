import React from "react";
import "./WeatherPanel.css"

class WeatherPanel extends React.Component {

    render() {
        return (
            <div className="infoDiv">
                <h3>Indicadores actuales</h3>
                <table className="weatherTable">
                    <tbody>
                        <tr>
                            <th><img className="icon" alt="" src={require('../../img/icons/temp.png')}/></th>
                            <th>Temperatura:<br/>{this.props.actuales.temperatura === null ? "No disponible":this.props.actuales.temperatura+" º"}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/humidity.png')}/></th>
                            <th>Humedad:<br/>{this.props.actuales.humedad === null ? "No disponible":this.props.actuales.humedad+" %"}</th>
                        </tr>
                        <tr>
                            <th><img className="icon" alt="" src={require('../../img/icons/windVelocity.png')}/></th>
                            <th>Velocidad viento:<br/>{this.props.actuales.velocidadviento === null ? "No disponible":this.props.actuales.velocidadviento+" Km/h"}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/windDirection.png')}/></th>
                            <th>Dirección viento:<br/>{this.props.actuales.direccionviento  === null ? "No disponible": this.props.actuales.direccionviento}</th>
                        </tr>
                        <tr>
                            <th><img className="icon" alt="" src={require('../../img/icons/rain.png')}/></th>
                            <th>Lluvia:<br/>{this.props.actuales.lluvia  === null ? "No disponible":this.props.actuales.lluvia+" %"}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/sunRadiation.png')}/></th>
                            <th>Radiación solar:<br/>{this.props.actuales.radiacionsolar  === null ? "No disponible":this.props.actuales.radiacionsolar+" W/m²"}</th>
                        </tr>
                        <tr>
                            <th><img className="icon" alt=""src={require('../../img/icons/sunUV.png')}/></th>
                            <th>Indice UV:<br/>{this.props.actuales.indiceuv  === null ? "No disponible":this.props.actuales.indiceuv+" uv"}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/evaporation.png')}/></th>
                            <th>Evaporación:<br/>{this.props.actuales.evaporacion  === null ? "No disponible":this.props.actuales.evaporacion +" mm/día"}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default WeatherPanel;