import React from "react";
import "./RightPanel.css"
import WeatherPanel from "../weatherPanel/WeatherPanel";
import Calendar from 'react-calendar';

class RightPanel extends React.Component {
    constructor() {
        super();
        this.state = {
            daySelected: ''
        };
    }
    onChange = value => this.setState({ daySelected: value.getDate() })
    render() {
        
        var count = 0;
        if (document.getElementsByClassName("calendar")[0] !== undefined) {
            var dayList = document.getElementsByClassName("calendar")[0].getElementsByClassName("react-calendar__viewContainer")[0]
                .getElementsByClassName("react-calendar__month-view")[0].getElementsByClassName("react-calendar__month-view__days")[0]
                .getElementsByClassName("react-calendar__tile react-calendar__month-view__days__day")
            for (let index = 0; index < dayList.length; index++) {
                if (this.props.monthdata[count] !== undefined) {
                    dayList[index].style.backgroundColor = getColor(this.props.monthdata[count].density);
                }
                count++;             
            }
        }

        return (
            <div className="rightPanelDiv">
                <WeatherPanel actuales={this.props.actuales}></WeatherPanel>
                <hr className="hr" />
                <h3 className="h3">Indicadores mensuales</h3>
                <Calendar className="calendar" activeStartDate={new Date(Date.now())} onChange={this.onChange} showNeighboringMonth={false} />
                <table className="monthTable">
                    <tbody>
                         <tr>
                            <th><img className="icon"  alt="" src={require('../../img/icons/occupation.png')}/></th>
                            <th>Ocupación máxima:<br/>{this.props.monthdata[this.state.daySelected-1]  !== undefined ? (this.props.monthdata[this.state.daySelected-1].density !== -1? this.props.monthdata[this.state.daySelected-1].density + " %":"No disponible"):""}</th>
                        </tr>
                        <tr>
                            <th><img className="icon" alt="" src={require('../../img/icons/tempMin.png')}/></th>
                            <th>Temperatura mínima:<br/>{this.props.monthdata[this.state.daySelected-1]  !== undefined ? (this.props.monthdata[this.state.daySelected-1].tempMin !== "NAN"? this.props.monthdata[this.state.daySelected-1].tempMin + " º":"No disponible"):""}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/tempMax.png')}/></th>
                            <th>Temperatura máxima:<br/>{this.props.monthdata[this.state.daySelected-1]  !== undefined ? (this.props.monthdata[this.state.daySelected-1].tempMax !== "NAN"? this.props.monthdata[this.state.daySelected-1].tempMax + " º":"No disponible"):""}</th>
                        </tr>
                        <tr>
                            <th><img className="icon" alt="" src={require('../../img/icons/humidityMin.png')}/></th>
                            <th>Humedad mínima:<br/>{this.props.monthdata[this.state.daySelected-1]  !== undefined ? (this.props.monthdata[this.state.daySelected-1].humMin !== "NAN"? this.props.monthdata[this.state.daySelected-1].humMin + " %":"No disponible"):""}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/humidityMax.png')}/></th>
                            <th>Humedad máxima:<br/>{this.props.monthdata[this.state.daySelected-1]  !== undefined ? (this.props.monthdata[this.state.daySelected-1].humMax !== "NAN"? this.props.monthdata[this.state.daySelected-1].humMax + " %":"No disponible"):""}</th>
                        </tr>
                        <tr>
                            <th><img className="icon" alt="" src={require('../../img/icons/rain.png')}/></th>
                            <th>Lluvia:<br/>{this.props.monthdata[this.state.daySelected - 1] !== undefined ? (this.props.monthdata[this.state.daySelected - 1].rain !== "NAN" ? this.props.monthdata[this.state.daySelected - 1].rain + " %" : "No disponible") : ""}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/evaporation.png')}/></th>
                            <th>Evaporación máxima:<br/>{this.props.monthdata[this.state.daySelected - 1] !== undefined ? (this.props.monthdata[this.state.daySelected - 1].evapMax !== "NAN" ? this.props.monthdata[this.state.daySelected - 1].evapMax + " mm/día" : "No disponible") : ""}</th>
                        </tr>
                        <tr>
                        <th><img className="icon" alt="" src={require('../../img/icons/sunRadiation.png')}/></th>
                            <th>Radiación solar máxima:<br/>{this.props.monthdata[this.state.daySelected - 1] !== undefined ? (this.props.monthdata[this.state.daySelected - 1].radMax !== "NAN" ? this.props.monthdata[this.state.daySelected - 1].radMax + " W/m²" : "No disponible") : ""}</th>
                            <th><img className="icon" alt="" src={require('../../img/icons/sunUV.png')}/></th>
                            <th>Indice UV máximo:<br/>{this.props.monthdata[this.state.daySelected - 1] !== undefined ? (this.props.monthdata[this.state.daySelected - 1].uvMax !== "NAN" ? this.props.monthdata[this.state.daySelected - 1].uvMax + " uv" : "No disponible") : ""}</th>

                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default RightPanel;
function getColor(d) {
    return d > 75 ? 'Red' :
        d > 50 ? 'Orange' :
            d > 25 ? 'Yellow' :
                d === -1 ?
                    'white' :
                    'Green';
}