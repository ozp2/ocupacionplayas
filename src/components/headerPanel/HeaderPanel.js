import React from "react";
import "./HeaderPanel.css"

class HeaderPanel extends React.Component {

    render() {
        return (
            <div className="headerPanel">
                <h2 className = "myH2">Torrevieja - Los Locos</h2>
            </div>
        )
    }
}

export default HeaderPanel;