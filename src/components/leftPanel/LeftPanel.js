import React from "react";
import "./LeftPanel.css"
import DateTimePicker from "react-datetime-picker";
import Select from "react-dropdown-select";
import { RadioGroup, RadioButton } from 'react-radio-buttons';

class LeftPanel extends React.Component {

    selectedValue = {}

    constructor() {
        super();
        this.state = {
            selectedValue: this.selectedValue,
        };
    }

    handleChange(value) {
        if (this.state != null) {
            this.setState({ selectedValue: value });
        }

    }
    render() {
        return (
            <div className="leftPanelDiv">
                <ul>
                    <br></br><li className="separationBetweenLabelComp">Cámara:</li>
                    <Select className="select" value={this} placeholder="Seleccionar..." options={this.props != null ? this.props.cameraList : ""} values={this.props != null ? Array.of(this.props.cameraList[0]) : []}
                        labelField="label" valueField="value" searchable="true" searchBy="label" clearOnSelect="false" onChange={(values) => this.props.onchangeCamaraId(values)} required />
                    <br></br><li className="separationBetweenLabelComp">Fecha de inicio:</li>
                    <li><DateTimePicker className="datepicker" onChange={(value) => this.props.onChangeStartDate(value)} value={this.props.tempStartDate} /></li>
                    <br></br><li className="separationBetweenLabelComp">Fecha de fin:</li>
                    <li><DateTimePicker className="datepicker" onChange={(value) => this.props.onChangeEndDate(value)} value={this.props.tempEndDate} /></li>
                </ul><br></br>
                <hr className="hrLeft hr"/>
                <p className="p">Mostrar histórico de zonas con indicador:</p>
                <RadioGroup className= "radioHistoricGroup" onChange={(value) => this.props.onRadioChange(value)} value={this.props.optHistoricalChart}>
                    <RadioButton className= "radioTemp" value="temperatura">Temperatura</RadioButton>
                    <RadioButton className= "radioRain" value="lluvia">Lluvia</RadioButton>
                    <RadioButton className= "radioHumidity" value="humedad">Humedad</RadioButton>
                </RadioGroup>
                <button className="myButton" onClick={this.props.buttonOnClick}>Actualizar</button>
            </div>
        )
    }
}

export default LeftPanel;