import React from "react";
import Control from 'react-leaflet-control';
import "./LegendPanel.css"

class LegendPanel extends React.Component {

    render() {

        return (
            <Control className = "legend" position="bottomright">
                <div className = "infoLegend">
                    <li>
                        <ul><i style={{background: 'green'}}>&nbsp;</i> 0 &ndash; 25 %</ul>
                        <ul><i style={{background: 'yellow'}}>&nbsp;</i> 25 &ndash; 50 %</ul>
                        <ul><i style={{background: 'orange'}}>&nbsp;</i> 50 &ndash; 75 %</ul>
                        <ul><i style={{background: 'red'}}>&nbsp;</i> +75 %</ul>
                    </li>
                </div>
            </Control>
        )

    }

}

export default LegendPanel;