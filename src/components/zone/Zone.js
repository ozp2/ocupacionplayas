import React from "react";
import { Polygon, Popup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { Bar } from 'react-chartjs-2';
import "./Zone.css"

class Zone extends React.Component {

    data = {
        labels: [],
        datasets: [
            {
                label: 'Histórico de ocupación',
                backgroundColor: 'rgba(75,192,192,0.4)',
                data: []
            }
        ]
    };

    dataTemperature = {
        labels: [],
        datasets: [
            {
                label: 'Histórico de temperatura',
                backgroundColor: 'rgba(230, 146, 10, 1)',
                data: []
            }
        ]
    };

    dataRain = {
        labels: [],
        datasets: [
            {
                label: 'Histórico de lluvia',
                backgroundColor: 'rgba(230, 146, 10, 1)',
                data: []
            }
        ]
    };

    dataHumidity = {
        labels: [],
        datasets: [
            {
                label: 'Histórico de humedad',
                backgroundColor: 'rgba(230, 146, 10, 1)',
                data: []
            }
        ]
    };

    constructor() {
        super();
        this.state = {
            data: this.data,
            dataTemperature: this.dataTemperature,
            dataRain: this.dataRain,
            dataHumidity: this.dataHumidity
        };
    }

    componentDidMount() {
        if (this.props.record !== undefined) {
            let data = { ...this.state.data };
            data.labels = this.props.record.date;
            data.datasets[0].data = this.props.record.occupation;
            this.setState({ data });
            
            let dataTemperature = { ...this.state.dataTemperature };
            dataTemperature.labels = this.props.record.date;
            dataTemperature.datasets[0].data = this.props.record.temperature;
            this.setState({ dataTemperature });

            let dataRain = { ...this.state.dataRain };
            dataRain.labels = this.props.record.date;
            dataRain.datasets[0].data = this.props.record.rain;
            this.setState({ dataRain });

            let dataHumidity = { ...this.state.dataHumidity };
            dataHumidity.labels = this.props.record.date;
            dataHumidity.datasets[0].data = this.props.record.humidity;
            this.setState({ dataHumidity });
        }
    }

    render() {

        const options = {
            scaleShowGridLines: true,
            scaleGridLineWidth: 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true
        }

        var positions = [[this.props.lat11, this.props.lng11], [this.props.lat12, this.props.lng12], [this.props.lat22, this.props.lng22], [this.props.lat21, this.props.lng21]]

        var dataBar2 = this.props.optHistoricalChart === 'temperatura'? this.state.dataTemperature : this.props.optHistoricalChart === 'lluvia' ? this.state.dataRain
            : this.state.dataHumidity;
        return (
            <Polygon positions={positions} color={getColor(this.props.density)} name={this.props.zoname} onMouseOver={(e) => {
                e.target.openPopup();
            }}
                onMouseOut={(e) => {
                    e.target.closePopup();
                }}>
                {
                    <Popup className="popup" height={700} minWidth="700">
                        {'Zona ' + this.props.zoname}<br />
                        {'Ocupación actual: ' + this.props.density + ' %'}
                        <ul>
                            <li><Bar className="bar" data={this.state.data} height={300} options={options} /></li>
                            <li><Bar className="bar" data={dataBar2} height={300} options={options} /></li>
                        </ul>
                    </Popup>


                }
            </Polygon>

        )

    }

}

export default Zone;

function getColor(d) {
    return d > 75 ? 'Red' :
        d > 50 ? 'Orange' :
            d > 25 ? 'Yellow' :
                'Green';
}