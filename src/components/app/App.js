import React from 'react';
import './App.css';
import MapView from '../map/MapView';
import HeaderPanel from '../headerPanel/HeaderPanel';
import FooterPanel from '../footerPanel/FooterPanel';

function App() {
  return (
    <div className="App">
      <HeaderPanel></HeaderPanel>
      <MapView/>
      <FooterPanel></FooterPanel>
    </div>
  );
}

export default App;
