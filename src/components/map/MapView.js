import React from "react";
import { Map, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "./MapView.css"
import Zone from '../zone/Zone';
import * as Const from '../../constants';
import { ToastsContainer, ToastsStore } from 'react-toasts';
import LegendPanel from "../legendPanel/LegendPanel";
import HeatmapLayer from 'react-leaflet-heatmap-layer';
import Switch from "react-switch";
import RightPanel from "../rightPanel/RightPanel";
import Control from 'react-leaflet-control';
import LeftPanel from "../leftPanel/LeftPanel";

var thisMapView = null;
class MapView extends React.Component {

    camaraList = [
        { value: '', label: '' }
    ]

    actuales = {
        temperatura: '',
        humedad: '',
        velocidadviento: '',
        direccionviento: '',
        lluvia: '',
        intensidadlluvia: '',
        indiceuv: '',
        radiacionsolar: '',
        evaporacion: ''
    };

    monthdata = [
        {
            day: '',
            density: '',
            tempMin: '',
            tempMax: '',
            presMin: '',
            presMax: '',
            humMin: '',
            humMax: '',
            evapMax: '',
            radMax: '',
            uvMax: '',
            rain: ''
        }
    ];

    heatmap = [
        {
            latitude: '',
            longitude: '',
            density: ''
        }
    ];

    densityRecord = [
        {
            date: [],
            occupation: [],
            temperature: [],
            humidity: [],
            rain: []

        }
    ];


    constructor() {
        super();
        this.state = {
            zones: [],
            densityByZones: {
                zoneDataList: [],
                totalDensity: '',
                dateTime: ''
            },
            startdate: null,
            enddate: null,
            tempStartDate: null,
            tempEndDate: null,
            camaraId: '',
            mastral: this.mastral,
            actuales: this.actuales,
            heatmap: this.heatmap,
            monthdata: this.monthdata,
            camaraList: this.camaraList,
            checkedSwitch: false,
            optHistoricalChart: 'temperatura'
        };
        this.handleChangeSwitch = this.handleChangeSwitch.bind(this);
    }

    onChangeStartDate = startdate => this.setState({ tempStartDate: startdate });
    onChangeEndDate = enddate => this.setState({ tempEndDate: enddate });
    onRadioChange = option => this.setState({ optHistoricalChart: option });
    onchangeCamaraId(values) {
        let camaraId = { ...thisMapView.state.camaraId };
        camaraId = values[0].value
        thisMapView.setState({ camaraId });
    }

    componentDidMount() {
        this.getBeaches();
        thisMapView = this;
    }

    handleChangeSwitch(checkedSwitch) {
        this.setState({ checkedSwitch });
    }

    applyFilter() {
        thisMapView.setState({ startdate: thisMapView.state.tempStartDate }, () => {
            thisMapView.setState({ enddate: thisMapView.state.tempEndDate }, () => {
                thisMapView.getData();
                thisMapView.getHeatMap();
                thisMapView.getMastralByDate();
                thisMapView.getMonthData();
                thisMapView.getDensityRecordByZone();
            });
        });
    }

    getData() {
        var filterDataByZone = '';
        if (this.state.startdate != null) {
            filterDataByZone = '?initdatetime=' + Math.round(this.state.startdate.getTime() / 1000);
        }
        if (this.state.enddate != null) {
            filterDataByZone = this.state.startdate != null ? filterDataByZone + '&enddatetime=' + Math.round(this.state.enddate.getTime() / 1000)
                : '?enddatetime=' + Math.round(this.state.enddate.getTime() / 1000)
        }
        if (this.state.camaraId !== '') {
            fetch(Const.DATA_URL + this.state.camaraId + filterDataByZone).then(res => res.json())
                .then(json => {
                    this.setState({ densityByZones: json })
                    if (json.totalDensity == null) {
                        ToastsStore.error("No se han encontrado datos para las fechas seleccionadas");
                    }
                });
        }
    }

    getDensityRecordByZone() {
        fetch(Const.DENSITY_RECORD_ZONE_URL + this.state.camaraId + "?record=10").then(res => res.json()).then(json => this.setState({ densityRecord: json }));
    }

    getMonthData() {
        fetch(Const.MONTH_DATA_URL + this.state.camaraId).then(res => res.json()).then(json => this.setState({ monthdata: json }));
    }

    getMastralByDate() {
        var filterDataByZone = '';
        if (this.state.startdate != null) {
            filterDataByZone = '?initdatetime=' + Math.round(this.state.startdate.getTime() / 1000);
        }
        if (this.state.enddate != null) {
            filterDataByZone = this.state.startdate != null ? filterDataByZone + '&enddatetime=' + Math.round(this.state.enddate.getTime() / 1000)
                : '?enddatetime=' + Math.round(this.state.enddate.getTime() / 1000)
        }
        fetch(Const.MASTRAL_BY_DATE_URL + this.state.camaraId + filterDataByZone).then(res => res.json()).then(json => this.setState({ actuales: json }));
    }

    getHeatMap() {
        var filterDataByZone = '';
        if (this.state.startdate != null) {
            filterDataByZone = '?initdatetime=' + Math.round(this.state.startdate.getTime() / 1000);
        }
        if (this.state.enddate != null) {
            filterDataByZone = this.state.startdate != null ? filterDataByZone + '&enddatetime=' + Math.round(this.state.enddate.getTime() / 1000)
                : '?enddatetime=' + Math.round(this.state.enddate.getTime() / 1000)
        }
        if (this.state.camaraId !== '') {
            fetch(Const.HEAT_MAP_URL + this.state.camaraId + filterDataByZone).then(res => res.json()).then(json => this.setState({ heatmap: json }, () => {
            }));
        }
    }

    getBeaches() {
        fetch(Const.BEACHES_URL).then(res => res.json()).then(json => this.setState({ camaraList: json }, () => {
            this.setState({ camaraId: this.state.camaraList[0].value }, () => {
                this.getData();
                this.getMonthData();
                this.getHeatMap();
                this.getDensityRecordByZone();
                this.getMastralByDate();
            });
        }));

    }



    render() {
        var zones;
        var dateActualData = "";

        if (this.state.densityByZones !== null && this.state.densityByZones.zoneDataList !== null && this.state.camaraId !== '') {
            dateActualData = new Date(Date.parse(this.state.densityByZones.dateTime));

            zones = this.state.densityByZones.zoneDataList.map((c, i) => <
                Zone key={i} zoname={c.zoneList.zoname}
                density={c.density}
                lat11={c.zoneList.lat11} lng11={c.zoneList.lng11}
                lat12={c.zoneList.lat12} lng12={c.zoneList.lng12}
                lat22={c.zoneList.lat22} lng22={c.zoneList.lng22}
                lat21={c.zoneList.lat21} lng21={c.zoneList.lng21}
                record={this.state.densityRecord[i]}
                optHistoricalChart={this.state.optHistoricalChart}
            />);
        }

        const gradient = {
            0.1: '#00ffa2', 0.2: '#00ffa2', 0.4: '#00ffa2',
            0.6: '#f2ff00', 0.8: '#ff8800', '1.0': '#ff0000'
        };

        return (

            <div className="mapDiv">
                <LeftPanel className="leftPanel" cameraList={this.state.camaraList} onchangeCamaraId={this.onchangeCamaraId} buttonOnClick={this.applyFilter}
                    onChangeStartDate={this.onChangeStartDate} onChangeEndDate={this.onChangeEndDate}
                    tempStartDate={this.state.tempStartDate} tempEndDate={this.state.tempEndDate} onRadioChange={this.onRadioChange} optHistoricalChart={this.state.optHistoricalChart}/>

                <Map className="myMap" center={Const.POSITION} zoom={Const.ZOOM} onClick={this.addPopup}>
                    <TileLayer url={Const.URL_MAP} attribution={Const.ATRIBUTION} />

                    {this.state.checkedSwitch ?
                        <HeatmapLayer
                            points={this.state.heatmap}
                            longitudeExtractor={m => m.longitude}
                            latitudeExtractor={m => m.latitude}
                            gradient={gradient}
                            max={1000.0}
                            intensityExtractor={m => parseFloat(m.density)}
                        />
                        :
                        zones
                    }
                    <ToastsContainer store={ToastsStore} />
                     <LegendPanel />
              
                    <Control className="heatmapSwitch" position="bottomleft">
                        <h2>Mapa de calor</h2>
                        <Switch onChange={this.handleChangeSwitch} checked={this.state.checkedSwitch} />
                    </Control>
                    <Control className="lastUpdate" position="topright">
                        <ul>
                            <li><h2>Última actualización: {dateActualData !== "" ? dateActualData.getDate() + "/" 
                            + (dateActualData.getMonth() + 1) 
                            + "/" + dateActualData.getFullYear() +"\t\t"+dateActualData.getHours()+":"+dateActualData.getMinutes() : "" 
                            }</h2></li>
                            <li><h2>Ocupación promedio: {this.state.densityByZones.totalDensity !== null ? this.state.densityByZones.totalDensity +"%":""}</h2></li>
                        </ul>
                    </Control>
                </Map>
                <RightPanel actuales={this.state.actuales} monthdata={this.state.monthdata} className="rightPanel" />
            </div>
        )
    }
}

export default MapView;